#include "datasource.h"

#include <QtCharts/QXYSeries>
#include <QtCharts/QValueAxis>
#include <QtConcurrent>
#include <QtCore/QDebug>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>
#include <QtCore/QRegularExpression>
#include <QtCore/QScopedPointer>
#include <QtCore/QtMath>
#include <QMutexLocker>

QT_CHARTS_USE_NAMESPACE

Q_DECLARE_METATYPE(QAbstractSeries *)
Q_DECLARE_METATYPE(QAbstractAxis *)

static const int MAX_SECONDS = 60;

static void addSample(DataSet &set, DataAxis &axis, qreal sample, qint64 ms, bool clearAll=false)
{
  QMutexLocker setLock(set.mutex);
  QMutexLocker axisLock(axis.mutex);

  auto &points = set.points;

  qreal y_axis_min = qMin(0., sample);
  qreal y_axis_max = qMax(0., sample);

  for (auto it = points.begin(); it != points.end(); ++it) {
    it->setX(it->x() - ms * 0.001);
    if (clearAll || (abs(ceil(it->x())) > MAX_SECONDS)) {
      points.erase(it, points.end());
      break;
    }

    qreal y = it->y();
    if (y < y_axis_min) {
      y_axis_min = y;
    }
    if (y > y_axis_max) {
      y_axis_max = y;
    }
  }

  points.insert(0, QPointF(0, sample));

  if (set.series && set.visible) {
    // TODO: This should zoom in again
    if (y_axis_min < axis.min) {
      axis.min = y_axis_min * 1.1;
      axis.dirty = true;
    }
    if (y_axis_max > axis.max) {
      axis.max = y_axis_max * 1.1;
      axis.dirty = true;
    }
  }
  set.dirty = true;
}

AlarmObject::AlarmObject(QString message, QDateTime since, QObject *parent)
: QObject(parent)
, _message(message)
, _since(since)
, _until(_since)
{}

BroadcastReceiver::BroadcastReceiver(QString host, quint16 port, DataSource &source, QObject *parent)
: QObject(parent)
, _host(host)
, _port(port)
, _source(source)
{
  qRegisterMetaType<QAbstractSocket::SocketError>();

  connect(&_sock, &QAbstractSocket::connected, this, &BroadcastReceiver::connected);
  connect(&_sock, &QAbstractSocket::disconnected, this, &BroadcastReceiver::disconnected);
  connect(&_sock, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &BroadcastReceiver::error);
  connect(&_sock, &QIODevice::readyRead, this, &BroadcastReceiver::receive);
}

void BroadcastReceiver::connectToServer()
{
  if (_sock.state() == QAbstractSocket::UnconnectedState) {
    qDebug() << "Socket is disconnected, trying to reconnect";
    _sock.connectToHost(_host, _port);
  }
}

void BroadcastReceiver::disconnectFromServer()
{
  _sock.abort();
}

void BroadcastReceiver::receive()
{
  _source.pushBuffer(_sock.readAll());
}

BroadcastParser::BroadcastParser(DataSets &sets, DataAxes &axes, DataSource &source, QObject *parent)
: QThread(parent)
, _source(source)
, _sets(sets)
, _axes(axes)
{}

void BroadcastParser::run()
{
  while (isRunning()) {
    QByteArray buffer(_source.popBuffer());

    for (auto &axis : _axes) {
      QMutexLocker axisLock(axis.mutex);
      axis.min = axis.minOrig;
      axis.max = axis.maxOrig;
    }

    QJsonDocument document = QJsonDocument::fromJson(buffer);
    if (document.isNull()) {
      qDebug() << "ERROR: Got invalid document";
      qDebug() << buffer;
      continue;
    }
    QJsonObject json = document.object();
    QString type = document["type"].toString();
    if (type == "broadcast") {
      parseBroadcast(json);
    } else if (type == "READBACK") {
      parseReadback(json);
    }
  }
}

//#define VICTOR_DATA

void BroadcastParser::parseBroadcast(QJsonObject &json)
{
  QJsonObject sensors = json["sensors"].toObject();
  QJsonValue timestamp = sensors["timestamp"];
  // Timestamp must be treated as a double as it is a uint32 on the
  // microcontroller and Qt can only handle int32 for a JSON int
  if (!timestamp.isDouble()) {
    qDebug() << "Error: timestamp has not been parsed as an int" << timestamp;
    return;
  }

  qint64 timestampCurrent = qRound64(timestamp.toDouble());
  const qint64 MAX_JUMP_BACK_WINDOW_MS = 1000;
  bool clearAll = false;
  if ((timestampCurrent >= std::pow(2, 32) - MAX_JUMP_BACK_WINDOW_MS) && (_timestampPrevious < MAX_JUMP_BACK_WINDOW_MS)) {
    timestampCurrent -= std::pow(2, 32);
  }
  if (timestampCurrent < _timestampPrevious) {
    qDebug() << "Recieved out of order data ("
              << "previous timestamp =" << _timestampPrevious
              << "current timestamp =" << timestampCurrent << ")";
    if (_timestampPrevious >= std::pow(2, 32) - MAX_JUMP_BACK_WINDOW_MS) {
      qDebug() << "Detected integer overflow, wrapping...";
      _timestampPrevious -= std::pow(2, 32);
    } else if ((timestampCurrent == 0) || (_timestampPrevious - timestampCurrent) > MAX_JUMP_BACK_WINDOW_MS) {
      qDebug() << "    Discarding out of order data";
      qDebug() << "    More than 1 second behind, assuming clock has reset";
      clearAll = true;
    } else {
      return;
    }
  }

  if (_timestampPrevious == std::numeric_limits<qint64>::min()) {
    qDebug() << "_timestampPrevious is zero, setting to" << timestampCurrent << "and skipping";
    _timestampPrevious = timestampCurrent;
    return;
  }

  qint64 ms = (timestampCurrent - _timestampPrevious);
  // qDebug() << "Time difference between now and the last point is =" << timestampCurrent << _timestampPrevious << ms;

  for (auto it = sensors.constBegin(); it != sensors.constEnd(); ++it) {
    QString key = it.key();
    if (key == "version") {
      // not shown atm
#ifdef VICTOR_DATA
    } else if (key == "pressure_inhale") {
      DataSet &set = _sets["volume"];
      DataAxis &yAxis = _axes[set.axisName];
      addSample(set, yAxis, it.value().toDouble(), ms, clearAll);
    } else if (key == "pressure_diff_patient") {
      // Ignore and overwrite with value below
    } else if (key == "temperature_buffer") {
      DataSet &set = _sets["pressure_diff_patient"];
      DataAxis &yAxis = _axes[set.axisName];
      addSample(set, yAxis, it.value().toDouble(), ms, clearAll);
#endif
    } else if (key == "fsm_state") {
      QString str = it.value().toString();
      _source.setFsmState(str.length() ? str : QString::number(it.value().toInt()));
    } else if (key == "timestamp") {
    } else {
      DataSet &set = _sets[key];
      DataAxis &yAxis = _axes[set.axisName];
      addSample(set, yAxis, it.value().toDouble(), ms, clearAll);
    }
  }
  _timestampPrevious = timestampCurrent;
}

void BroadcastParser::parseReadback(QJsonObject &json)
{
  QJsonObject readback = json["READBACK"].toObject();
  for (auto it = readback.constBegin(); it != readback.constEnd(); ++it) {
    QString key = it.key();
    if (key == "duration_inhale") {
      _source.setInhaleTime(it.value().toInt());
    } else if (key == "duration_pause") {
      _source.setPauseTime(it.value().toInt());
    } else if (key == "duration_exhale") {
      _source.setExhaleTime(it.value().toInt());
    } else if (key == "peep") {
      _source.setPeep(it.value().toInt());
    } else if (key == "valve_air_in") {
      _source.setValveInAir(it.value().toInt());
    } else if (key == "valve_inhale") {
      _source.setValveInhale(it.value().toInt());
    } else if (key == "valve_o2_in") {
      _source.setValveInO2(it.value().toInt());
    } else if (key == "valve_exhale") {
      _source.setValveExhale(it.value().toInt());
    }
  }
}

CommandObject::CommandObject(QString name, QString host, quint16 port, QJsonDocument json, QObject *parent)
: QObject(parent)
, _name(name)
, _json(json)
, _acked(false)
{
  connect(&_sock, &QAbstractSocket::connected, this, &CommandObject::connected);
  connect(&_sock, &QAbstractSocket::disconnected, this, &CommandObject::disconnected);

  _sock.connectToHost(host, port);
}

void CommandObject::connected()
{
  connect(&_sock, &QAbstractSocket::readyRead, this, &CommandObject::replied);

  _sock.write(_json.toJson(QJsonDocument::Compact));

  emit sent(_name);
}

void CommandObject::disconnected()
{
  if (_acked) {
    emit acked(_name);
  } else {
    emit failed(_name);
  }
}

void CommandObject::replied()
{
  QByteArray buffer = _sock.readAll();

  QJsonDocument document = QJsonDocument::fromJson(buffer);
  QJsonObject json = document.object();

  if (json["type"].toString() == "ack") {
    _acked = true;
  }
  _sock.disconnectFromHost();
}

DataSource::DataSource(QString host, quint16 port, QObject *parent)
: QObject(parent)
, _host(host)
, _port(port)
, _broadcastReceiver(host, port, *this)
, _broadcastParser(_sets, _axes, *this)
{
  qRegisterMetaType<QAbstractSeries *>();
  qRegisterMetaType<QAbstractAxis *>();

  _broadcastReceiverThread.setObjectName("BroadcastReceiver");
  _broadcastReceiver.moveToThread(&_broadcastReceiverThread);
  _broadcastParser.setObjectName("BroadcastParser");

  connect(&_updateTimer, &QTimer::timeout, this, &DataSource::update);
  connect(&_reconnectTimer, &QTimer::timeout, this, &DataSource::broadcastConnect);
  connect(&_broadcastReceiver, &BroadcastReceiver::connected, this, &DataSource::broadcastConnected);
  connect(&_broadcastReceiver, &BroadcastReceiver::disconnected, this, &DataSource::broadcastDisconnected);
  connect(&_broadcastReceiver, &BroadcastReceiver::error, this, &DataSource::broadcastError);
}

DataSource::~DataSource()
{
  _updateTimer.stop();

  _broadcastReceiverThread.exit();
  _broadcastParser.exit();
  _buffersNotEmpty.wakeAll();
  _broadcastParser.wait(1000);

  for (auto &set : _sets) {
    delete set.mutex;
  }
  for (auto &axis : _axes) {
    delete axis.mutex;
  }
  auto alarms = _alarms;
  _alarms.clear();
  emit alarmsChanged();
  for (auto &alarm : alarms) {
    delete alarm;
  }
}

static QAbstractSeries *findSeries(QObject *object, QString chartName, QString seriesName)
{
  QObject *chartObject = object->findChild<QObject *>(chartName);
  if (chartObject) {
    int count = chartObject->property("count").toInt();
    for (int i = 0; i < count; ++i) {
      QAbstractSeries *series;
      QMetaObject::invokeMethod(
       chartObject, "series", Qt::AutoConnection, Q_RETURN_ARG(QAbstractSeries *, series), Q_ARG(int, i));
      QString objName = series->objectName();
      if (objName == seriesName) {
        return series;
      }
    }
  }
  return nullptr;
}

void DataSource::setup(QObject *mainWindow)
{
  reset();

  setupChart(mainWindow, "pressureChart");
  setupChart(mainWindow, "flowChart");
  setupChart(mainWindow, "volumeChart");

  QAbstractSeries *mainPressure = findSeries(mainWindow, "mainPressureChart", "airway_pressure");
  if (mainPressure) {
    _sets["airway_pressure"].mainSeries = mainPressure;
  }
  QAbstractSeries *mainFlow = findSeries(mainWindow, "mainFlowChart", "flow");
  if (mainFlow){
    _sets["flow"].mainSeries = mainFlow;
  }
  QAbstractSeries *mainVolume = findSeries(mainWindow, "mainVolumeChart", "volume");
  if (mainVolume) {
    _sets["volume"].mainSeries = mainVolume;
  }

  _updateTimer.setTimerType(Qt::PreciseTimer);
  _updateTimer.start(40); //25 fps
  //_scrollTimer.restart();

  _broadcastReceiverThread.start();
  _broadcastParser.start();
  broadcastConnect();
}

void DataSource::reset()
{
  _sets.clear();
}

void DataSource::import(QFile &file)
{
  QTextStream txt(&file);
  DataSet &dataSetPressure = _sets["pressure_inhale"];
  DataSet &dataSetFlow = _sets["flow"];
  DataSet &dataSetVolume = _sets["volume"];
  DataAxis &yAxisPressure = _axes[dataSetPressure.axisName];
  DataAxis &yAxisFlow = _axes[dataSetFlow.axisName];
  DataAxis &yAxisVolume = _axes[dataSetVolume.axisName];

  float timePrev = -1;
  while (!txt.atEnd()) {
    QString line = txt.readLine();
    QRegularExpression re_csv(
     QLatin1String("([-0-9.]+),([-0-9.]+),([-0-9.]+),([-0-9.]+)"));
    QRegularExpressionMatch match;
    if (!line.contains(re_csv, &match)) {
      continue;
    }
    float time = match.captured(1).toFloat();
    float pressure = match.captured(2).toFloat();
    float flow = match.captured(3).toFloat();
    float volume = match.captured(4).toFloat();
    //qDebug() << time
    //         << pressure
    //         << flow
    //         << volume;
    if (timePrev < 0) timePrev = time;
    int ms = ceil((time - timePrev) * 1000.0);

    addSample(dataSetPressure, yAxisPressure, pressure, ms);
    addSample(dataSetFlow, yAxisFlow, flow, ms);
    addSample(dataSetVolume, yAxisVolume, volume, ms);

    timePrev = time;
  }
}

void DataSource::fakeAlarm(QString message, int minutesAgo, int minutesLong, bool acked)
{
  QDateTime since = QDateTime::currentDateTime();
  since = since.addSecs(-minutesAgo * 60);
  QDateTime until = since.addSecs(minutesLong * 60);
  qDebug() << "fakeAlarm: " << message;
  _alarms.prepend(new AlarmObject(message, since));
  _alarms.front()->setProperty("until", until);
  _alarms.front()->setProperty("acked", acked);
  emit alarmsChanged();
  emit alarm(message);
}

void DataSource::pushBuffer(QByteArray b)
{
  _buffersMutex.lock();
  _buffers.append(b);
  _buffersNotEmpty.wakeAll();
  _buffersMutex.unlock();
}

QByteArray DataSource::popBuffer()
{
  // Wait for a message to be recieved
  _buffersMutex.lock();
  auto index = _buffers.indexOf('\0');
  if (index < 0) {
    _buffersNotEmpty.wait(&_buffersMutex);
  }
  // If there is a backlog of messages, skip to the last one
  const int messageCount = _buffers.count('\0');
  if (messageCount > 10) {
    qDebug() << "WARNING: Message backlog of" << messageCount << "messages, skipping forwards";
    // Ignore the last byte as valid messages end with a null byte
    _buffers = _buffers.mid(_buffers.lastIndexOf('\0', -2) + 1);
  }
  // Find the message and return it
  QByteArray data;
  index = _buffers.indexOf('\0');
  if (index < 0 ) {
    data = nullptr;
  } else {
    data = _buffers.left(index);
    _buffers = _buffers.mid(index+1);
  }
  _buffersMutex.unlock();
  return data;
}

void DataSource::broadcastConnect()
{
  _broadcastReceiver.connectToServer();
}

void DataSource::broadcastConnected()
{
  qDebug() << "Connected to" << _host << ":" << _port;
  setConnected(true);
}

void DataSource::broadcastDisconnect()
{
  _broadcastReceiver.disconnectFromServer();
}

void DataSource::broadcastDisconnected()
{
  qDebug() << "Disconnected";

  setConnected(false);
  _reconnectTimer.start(1000);
}

void DataSource::broadcastError(QAbstractSocket::SocketError error)
{
  qDebug() << error;
  emit notification("Communication error");
  _reconnectTimer.start(1000);
}

//#define UPDATE_CONCURRENT

void DataSource::update()
{
  auto it = _commands.begin();
  while (it != _commands.end()) {
    if ((*it)->state() == QAbstractSocket::UnconnectedState) {
      delete *it;
      it = _commands.erase(it);
    } else {
      ++it;
    }
  }
#ifdef UPDATE_CONCURRENT
  auto future = QtConcurrent::map(_sets, [this](DataSet &set) {
#else
  for (auto &set : _sets) {
#endif
    if (set.mutex->tryLock()) {
      if (!set.series) {
        set.mutex->unlock();
#ifdef UPDATE_CONCURRENT
        return;
#else
        continue;
#endif
      }
      set.visible = set.series->isVisible();
      if (!set.dirty) {
        set.mutex->unlock();
#ifdef UPDATE_CONCURRENT
        return;
#else
        continue;
#endif
      }
      set.dirty = false;
      QXYSeries *xySeries = static_cast<QXYSeries *>(set.series);
      xySeries->replace(set.points);
      if (set.mainSeries) {
        QXYSeries *mainSeries = static_cast<QXYSeries *>(set.mainSeries);
        mainSeries ->replace(set.points);
      }
      set.mutex->unlock();
    }
    auto &axis = _axes[set.axisName];
    if (axis.mutex->tryLock()) {
      if (axis.dirty) {
        axis.dirty = false;
        auto plotAxes = set.series->attachedAxes();
        for (auto plotAxis : plotAxes) {
          if (!plotAxis->objectName().length()) {
            continue;
          }
          plotAxis->setMin(axis.min);
          plotAxis->setMax(axis.max);
          break; //stop at the first named axis
        }
      }
      axis.mutex->unlock();
    }
#ifdef UPDATE_CONCURRENT
  });
#else
  }
#endif
  //future.waitForFinished();
  //return _scrollTimer.restart();
}

void DataSource::ackAlarms()
{
  for (auto &alarm : _alarms) {
    alarm->setProperty("acked", true);
  }
  emit alarmsChanged();
}

void DataSource::resetAlarms()
{
  for (auto &alarm : _alarms) {
    alarm->setProperty("acked", false);
  }
  emit alarmsChanged();
}

void DataSource::sendStart()
{
  QJsonDocument json;
  QJsonObject cmd;
  cmd["type"] = "cmd";
  cmd["cmdtype"] = "GENERAL";
  cmd["cmd"] = "START";
  cmd["param"]; // null
  json.setObject(cmd);

  sendCommand("Start", json);
}

void DataSource::sendStop()
{
  QJsonDocument json;
  QJsonObject cmd;
  cmd["type"] = "cmd";
  cmd["cmdtype"] = "GENERAL";
  cmd["cmd"] = "STOP";
  cmd["param"]; // null
  json.setObject(cmd);

  sendCommand("Stop", json);
}

void DataSource::sendTimeout(QString descr, QString cmdname, int ms)
{
  QJsonDocument json;
  QJsonObject cmd;
  cmd["type"] = "cmd";
  cmd["cmdtype"] = "SET_TIMEOUT";
  cmd["cmd"] = cmdname;
  cmd["param"] = ms;
  json.setObject(cmd);

  sendCommand(descr, json);
}

void DataSource::sendCommand(QString name, QJsonDocument json)
{
  CommandObject *co = new CommandObject(name, _host, _port - 1, json, this);
  emit commandCreated(name);
  connect(co, &CommandObject::sent, this, &DataSource::commandSent);
  connect(co, &CommandObject::acked, this, &DataSource::commandAcked);
  connect(co, &CommandObject::failed, this, &DataSource::commandFailed);
  _commands.append(co);
}


void DataSource::setupChart(QObject *object, QString objectName)
{
  QObject *chartObject = object->findChild<QObject *>(objectName);
  if (chartObject) {
    //chartObject is DeclarativeChart *, an internal type that can't be accessed directly
    int count = chartObject->property("count").toInt();
    for (int i = 0; i < count; ++i) {
      QAbstractSeries *series;
      QMetaObject::invokeMethod(
       chartObject, "series", Qt::AutoConnection, Q_RETURN_ARG(QAbstractSeries *, series), Q_ARG(int, i));
      QString objName = series->objectName();
      if (!objName.length()) {
        qDebug() << "Series '" << series->name() << "does not have an objectName";
        continue;
      }
      auto plotAxes = series->attachedAxes();
      for (auto plotAxis : plotAxes) {
        if (!plotAxis->objectName().length()) {
          continue;
        }
        auto valueAxis = static_cast<QValueAxis *>(plotAxis);
        auto &axis = _axes[plotAxis->objectName()];
        axis.minOrig = valueAxis->min();
        axis.min = axis.minOrig;
        axis.maxOrig = valueAxis->max();
        axis.max = axis.maxOrig;
        axis.dirty = false;
        _sets[objName].axisName = plotAxis->objectName();
        _sets[objName].points.reserve(MAX_SECONDS * 1000);
        break; //stop at the first named axis
      }
      QXYSeries *xySeries = static_cast<QXYSeries *>(series);
      QPen pen = xySeries->pen();
      pen.setWidth(3);
      xySeries->setPen(pen);

      _sets[objName].series = series;
      _sets[objName].points.clear();
      _sets[objName].dirty = false;
      _sets[objName].visible = series->isVisible();
    }
  } else {
    qDebug() << "ChartView '" << objectName << "' not found";
  }
}

void DataSource::raiseAlarm(QString message)
{
  qDebug() << "alarm: " << message;
  emit alarm(message);

  // if this alarm has been recorded within the last minute, just extend that one

  AlarmObject *alarm = nullptr;
  for (auto it = _alarms.begin(); it != _alarms.end(); ++it) {
    alarm = qobject_cast<AlarmObject *>(*it);
    if (alarm->message() == message) {
      QDateTime now = QDateTime::currentDateTime();
      QDateTime until = alarm->until();
      if (until.secsTo(now) <= 60) {
        alarm->setUntil(now);
        alarm->setAcked(false);
        _alarms.erase(it);
        break;
      }
    }
  }
  _alarms.prepend(alarm ? alarm : new AlarmObject(message));

  emit alarmsChanged();
}
