import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    Rectangle {
        color: Style.btnDnColor
        anchors.fill: parent
    }

    GridLayout {
        anchors.fill: parent
        columns: 4
        Text {
            text: "Inhale time [ms]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: inhaleTime
            text: dataSource.inhaleTime
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
            onReleased: askInputDigits(inhaleTime, text)
            Keys.onReturnPressed: set(text)
            function set(value) {
                dataSource.sendTimeout("Inhale time", "INHALE", value)
            }
        }
        Text {
            text: "Valve in Air [%]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: valveInAir
            text: dataSource.valveInAir
            onReleased: askInputDigits(valveInAir, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        Text {
            text: "Fill time [ms]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: fillTime
            text: dataSource.fillTime
            onReleased: askInputDigits(fillTime, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        Text {
            text: "Valve in O2 [%]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: valveInO2
            text: dataSource.valveInO2
            onReleased: askInputDigits(valveInO2, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        Text {
            text: "Pause [ms]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: pauseTime
            text: dataSource.pauseTime
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
            onReleased: askInputDigits(pauseTime, text)
            Keys.onReturnPressed: set(text)
            function set(value) {
                dataSource.sendTimeout("Pause time", "PAUSE", value)
            }
        }
        Text {
            text: "Valve Inhale [%]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: valveInhale
            text: dataSource.valveInhale
            onReleased: askInputDigits(valveInhale, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        Text {
            text: "Exhale/Inhale [ms]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: exhaleInhale
            text: dataSource.exhaleInhale
            onReleased: askInputDigits(exhaleInhale, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        Text {
            text: "Valve Exhale [%]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: valveExhale
            text: dataSource.valveExhale
            onReleased: askInputDigits(valveExhale, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        Text {
            text: "PEEP [cmH2O]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: peep
            text: dataSource.peep
            onReleased: askInputDigits(peep, text)
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        RowLayout {
            Layout.columnSpan: 2
            Text {
                text: "State:"
                font.family: Style.fontFamily
                font.pointSize: Style.fontPointSize
            }
            TextField {
                text: dataSource.fsmState
                readOnly: true
                font.family: Style.fontFamily
                font.pointSize: Style.fontPointSize
                background: Rectangle {
                    color: Style.btnDnColor
                }
            }
        }
        Text {
            text: "Exhale time [ms]"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: exhaleTime
            text: dataSource.exhaleTime
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
            readOnly: true
            background: Rectangle {
                color: Style.btnDnColor
            }
        }
        RowLayout {
            Layout.rowSpan: 2
            Layout.columnSpan: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Button {
                text: "Inhale"
                font.family: Style.fontFamily
                font.pointSize: Style.fontPointSize
            }
            Image {
                source: "svg/lungs-solid.svg"
                sourceSize.height: 100
            }
            Button {
                text: "Exhale"
                font.family: Style.fontFamily
                font.pointSize: Style.fontPointSize
            }
        }
        Text {
            text: "RPM"
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
        }
        TextField {
            id: rpm
            text: dataSource.rpm
            font.family: Style.fontFamily
            font.pointSize: Style.fontPointSize
            readOnly: true
            background: Rectangle {
                color: Style.btnDnColor
            }
        }
    }
}
