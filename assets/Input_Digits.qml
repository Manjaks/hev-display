import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: inputDigits
    property double number: 0
    ColumnLayout {
        Row {
            Layout.alignment: Qt.AlignHCenter
            spacing: 10
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "-"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: {
                    var n = inputDigits.number - 1
                    field.text = n.toFixed(1)
                }
            }
            TextField {
                id: field
                horizontalAlignment: Text.AlignHCenter
                width: numPad.width
                height: Style.btnHeight
                placeholderText: "0"
                overwriteMode: true
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                validator: DoubleValidator {
                    //TODO: top
                    //TODO: bottom
                }
                onTextChanged: {
                    font.strikeout = text.length && !acceptableInput
                    if (acceptableInput) {
                        inputDigits.number = text
                    }
                }
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "+"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: {
                    var n = inputDigits.number + 1
                    field.text = n.toFixed(1)
                }
            }
        }
        Grid {
            id: numPad
            columns: 3
            rows: 4
            spacing: 10
            Layout.alignment: Qt.AlignHCenter
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "7"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "8"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "9"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "4"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "5"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "6"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "1"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "2"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "3"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                radius: Style.btnRadius
                Image {
                    anchors.centerIn: parent
                    source: "svg/backspace-solid.svg"
                    sourceSize.width: parent.height * 0.8
                }
                onClicked: field.text = field.text.slice(0, -1)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "0"
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
            RoundButton {
                width: Style.btnWidth
                height: Style.btnHeight
                text: "."
                radius: Style.btnRadius
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
                onClicked: type(text)
            }
        }
        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.topMargin: 10
            spacing: 10
            RoundButton {
                Layout.fillWidth: true
                Layout.minimumWidth: Style.btnHeight * 1.5
                Layout.minimumHeight: Style.btnHeight
                Image {
                    anchors.centerIn: parent
                    source: "svg/check-solid.svg"
                    sourceSize.height: parent.height * 0.8
                }
                onClicked: returnInputDigits(true, number)
            }
            RoundButton {
                Layout.fillWidth: true
                Layout.minimumWidth: Style.btnHeight * 1.5
                Layout.minimumHeight: Style.btnHeight
                Image {
                    anchors.centerIn: parent
                    source: "svg/times-solid.svg"
                    sourceSize.height: parent.height * 0.8
                }
                onClicked: returnInputDigits(false, number)
            }
        }
    }
    function initialize(input) {
        field.text = input
        field.selectAll()
    }
    function type(text) {
        if (field.selectedText.length) {
            field.text = field.text.substring(0, field.selectionStart)
                    + text + field.text.substring(field.selectionEnd)
        } else {
            field.text += text
        }
    }
}
