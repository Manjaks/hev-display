#ifndef DATASOURCE_H
#define DATASOURCE_H

#include <QtCore/QElapsedTimer>
#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QObject>
#include <QtCore/QThread>
#include <QtCore/QTime>
#include <QtCore/QTimer>
#include <QtCore/QWaitCondition>
#include <QtCharts/QAbstractSeries>
#include <QMutex>
#include <QTcpSocket>

QT_CHARTS_USE_NAMESPACE

struct DataSet
{
  QMutex *mutex = new QMutex(); //We never remove a dataset until termination
  QAbstractSeries *series = nullptr;
  QAbstractSeries *mainSeries = nullptr;
  QVector<QPointF> points;
  QString axisName;
  bool dirty = false;
  bool visible = true;
};
typedef QMap<QString, DataSet> DataSets;

struct DataAxis
{
  QMutex *mutex = new QMutex(); //We never remove an axis until termination
  qreal minOrig;
  qreal maxOrig;
  qreal min;
  qreal max;
  bool dirty = false;
};
typedef QMap<QString, DataAxis> DataAxes;

class AlarmObject : public QObject
{
  Q_OBJECT

  Q_PROPERTY(bool acked READ acked WRITE setAcked NOTIFY ackedChanged)
  Q_PROPERTY(QString message READ message NOTIFY messageChanged)
  Q_PROPERTY(QDateTime since READ since NOTIFY sinceChanged)
  Q_PROPERTY(QDateTime until READ until WRITE setUntil NOTIFY untilChanged)

signals:
  void ackedChanged();
  void messageChanged();
  void sinceChanged();
  void untilChanged();

public:
  AlarmObject(QString text, QDateTime since = QDateTime::currentDateTime(), QObject *parent = nullptr);

  bool acked() const { return _acked; }
  void setAcked(bool acked) {
    if (acked != _acked) {
      _acked = acked;
      emit ackedChanged();
    }
  }

  QString message() const { return _message; }
  QDateTime since() const { return _since; }

  QDateTime until() const { return _until; }
  void setUntil(QDateTime until) {
    if (until != _until) {
      _until = until;
      emit untilChanged();
    }
  }

private:
  bool _acked = false;
  const QString _message;
  const QDateTime _since;
  QDateTime _until;
};
typedef QList<AlarmObject *> AlarmObjects;

class DataSource;
class BroadcastReceiver : public QObject
{
  Q_OBJECT

public:
  explicit BroadcastReceiver(QString host, quint16 port, DataSource &source, QObject *parent = nullptr);

  void connectToServer();
  void disconnectFromServer();

signals:
  void connected();
  void disconnected();
  void error(QAbstractSocket::SocketError error);

public slots:
  void receive();

private:
  const QString _host;
  const quint16 _port;
  QTcpSocket _sock;
  DataSource &_source;
};

class BroadcastParser : public QThread
{
  Q_OBJECT

public:
  explicit BroadcastParser(DataSets &sets, DataAxes &axes, DataSource &source, QObject *parent = nullptr);

private:
  // Alternatively, this could be a slot triggered by the receiver (which actually appears to be a bit faster)
  void run() override;

  void parseBroadcast(QJsonObject &json);
  void parseReadback(QJsonObject &json);

private:
  qint64 _timestampPrevious = std::numeric_limits<qint64>::min();

  DataSource &_source;
  DataSets &_sets;
  DataAxes &_axes;
  QElapsedTimer _timer;
};

class CommandObject : public QObject
{
  Q_OBJECT

public:
  CommandObject(QString name, QString host, quint16 port, QJsonDocument json, QObject *parent = nullptr);

  QAbstractSocket::SocketState state() const { return _sock.state(); }

signals:
  void sent(QString name);
  void acked(QString name);
  void failed(QString name);

private slots:
  void connected();
  void replied();
  void disconnected();

private:
  const QString _name;
  QTcpSocket _sock;
  const QJsonDocument _json;
  bool _acked;
};

class DataSource : public QObject
{
  Q_OBJECT

  // Properties received from the server are read-only, properties settable by an operator are read-write
  // (in alphabetical order)
  Q_PROPERTY(QVariant alarms READ alarms NOTIFY alarmsChanged)
  Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
  Q_PROPERTY(qreal exhaleInhale READ exhaleInhale WRITE setExhaleInhale NOTIFY exhaleInhaleChanged)
  Q_PROPERTY(qreal exhaleTime READ exhaleTime NOTIFY exhaleTimeChanged)
  Q_PROPERTY(qreal fillTime READ fillTime WRITE setFillTime NOTIFY fillTimeChanged)
  Q_PROPERTY(QString fsmState READ fsmState NOTIFY fsmStateChanged)
  Q_PROPERTY(qreal inhaleTime READ inhaleTime WRITE setInhaleTime NOTIFY inhaleTimeChanged)
  Q_PROPERTY(QString mode READ mode WRITE setMode NOTIFY modeChanged)
  Q_PROPERTY(int patientAge READ patientAge WRITE setPatientAge NOTIFY patientAgeChanged)
  Q_PROPERTY(QString patientName READ patientName WRITE setPatientName NOTIFY patientNameChanged)
  Q_PROPERTY(QString patientSurname READ patientSurname WRITE setPatientSurname NOTIFY patientSurnameChanged)
  Q_PROPERTY(qreal patientWeight READ patientWeight WRITE setPatientWeight NOTIFY patientWeightChanged)
  Q_PROPERTY(qreal pauseTime READ pauseTime WRITE setPauseTime NOTIFY pauseTimeChanged)
  Q_PROPERTY(qreal peep READ peep NOTIFY peepChanged)
  Q_PROPERTY(qreal rpm READ rpm NOTIFY rpmChanged)
  Q_PROPERTY(qreal valveInAir READ valveInAir WRITE setValveInAir NOTIFY valveInAirChanged)
  Q_PROPERTY(qreal valveInhale READ valveInhale WRITE setValveInhale NOTIFY valveInhaleChanged)
  Q_PROPERTY(qreal valveInO2 READ valveInO2 WRITE setValveInO2 NOTIFY valveInO2Changed)
  Q_PROPERTY(qreal valveExhale READ valveExhale WRITE setValveExhale NOTIFY valveExhaleChanged)

public:
  explicit DataSource(QString host, quint16 port, QObject *parent = 0);
  virtual ~DataSource();

  // Append here signal getters/setters for every Q_PROPERTY that should be exposed to the UI
  // (in alphabetical order)
  QVariant alarms() const { return QVariant::fromValue(_alarms); }

  bool connected() const { return _connected; }
  void setConnected(bool connected) {
    if (connected != _connected) {
      _connected = connected;
      emit connectedChanged();
    }
  }

  qreal exhaleInhale() const { return _exhaleInhale; }
  void setExhaleInhale(qreal exhaleInhale) {
    if (exhaleInhale != _exhaleInhale) {
      _exhaleInhale = exhaleInhale;
      emit exhaleInhaleChanged();
    }
  }

  qreal exhaleTime() const { return _exhaleTime; }
  void setExhaleTime(qreal exhaleTime) {
    if (exhaleTime != _exhaleTime) {
      _exhaleTime = exhaleTime;
      emit exhaleTimeChanged();
    }
  }

  qreal fillTime() const { return _fillTime; }
  void setFillTime(qreal fillTime) {
    if (fillTime != _fillTime) {
      _fillTime = fillTime;
      emit fillTimeChanged();
    }
  }

  const QString &fsmState() const { return _fsmState; }
  void setFsmState(QString fsmState) {
    if (fsmState != _fsmState) {
      _fsmState = fsmState;
      emit fsmStateChanged();
    }
  }

  qreal inhaleTime() const { return _inhaleTime; }
  void setInhaleTime(qreal inhaleTime) {
    if (inhaleTime != _inhaleTime) {
      _inhaleTime = inhaleTime;
      emit inhaleTimeChanged();
    }
  }

  const QString &mode() const { return _mode; }
  void setMode(QString mode) {
    if (mode != _mode) {
      _mode = mode;
      emit modeChanged();
    }
  }

  int patientAge() const { return _patientAge; }
  void setPatientAge(int patientAge) {
    if (patientAge != _patientAge) {
      _patientAge = patientAge;
      emit patientAgeChanged();
    }
  }

  const QString &patientName() const { return _patientName; }
  void setPatientName(QString patientName) {
    if (patientName != _patientName) {
      _patientName = patientName;
      emit patientNameChanged();
    }
  }

  const QString &patientSurname() const { return _patientSurname; }
  void setPatientSurname(QString patientSurname) {
    if (patientSurname != _patientSurname) {
      _patientSurname = patientSurname;
      emit patientSurnameChanged();
    }
  }

  int patientWeight() const { return _patientWeight; }
  void setPatientWeight(int patientWeight) {
    if (patientWeight != _patientWeight) {
      _patientWeight = patientWeight;
      emit patientWeightChanged();
    }
  }

  qreal pauseTime() const { return _pauseTime; }
  void setPauseTime(qreal pauseTime) {
    if (pauseTime != _pauseTime) {
      _pauseTime = pauseTime;
      emit pauseTimeChanged();
    }
  }

  qreal rpm() const { return _rpm; }
  void setRpm(qreal rpm) {
    if (rpm != _rpm) {
      _rpm = rpm;
      emit rpmChanged();
    }
  }

  qreal peep() const { return _peep; }
  void setPeep(qreal peep) {
    if (peep != _peep) {
      _peep = peep;
      emit peepChanged();
    }
  }

  qreal valveInAir() const { return _valveInAir; }
  void setValveInAir(qreal valveInAir) {
    if (valveInAir != _valveInAir) {
      _valveInAir = valveInAir;
      emit valveInAirChanged();
    }
  }

  qreal valveInhale() const { return _valveInhale; }
  void setValveInhale(qreal valveInhale) {
    if (valveInhale != _valveInhale) {
      _valveInhale = valveInhale;
      emit valveInhaleChanged();
    }
  }

  qreal valveInO2() const { return _valveInO2; }
  void setValveInO2(qreal valveInO2) {
    if (valveInO2 != _valveInO2) {
      _valveInO2 = valveInO2;
      emit valveInO2Changed();
    }
  }

  qreal valveExhale() const { return _valveExhale; }
  void setValveExhale(qreal valveExhale) {
    if (valveExhale != _valveExhale) {
      _valveExhale = valveExhale;
      emit valveExhaleChanged();
    }
  }

  // End of Q_PROPERTY getters/setters

  void setup(QObject *mainWindow);
  void reset();

  void import(QFile &file);
  void fakeAlarm(QString message, int minutesAgo, int minutesLong, bool acked = false);

  void pushBuffer(QByteArray b);
  QByteArray popBuffer();

signals:
  void alarm(QString message);
  void commandCreated(QString name);
  void commandSent(QString name);
  void commandAcked(QString name);
  void commandFailed(QString name);
  void notification(QString message);

  // Append here signal emitters for every Q_PROPERTY that should be exposed to the UI
  // (in alphabetical order)
  void alarmsChanged();
  void connectedChanged();
  void exhaleInhaleChanged();
  void exhaleTimeChanged();
  void fillTimeChanged();
  void fsmStateChanged();
  void inhaleTimeChanged();
  void modeChanged();
  void patientAgeChanged();
  void patientNameChanged();
  void patientSurnameChanged();
  void patientWeightChanged();
  void pauseTimeChanged();
  void rpmChanged();
  void peepChanged();
  void valveInAirChanged();
  void valveInhaleChanged();
  void valveInO2Changed();
  void valveExhaleChanged();

public slots:
  void broadcastConnect();
  void broadcastConnected();
  void broadcastDisconnect();
  void broadcastDisconnected();
  void broadcastError(QAbstractSocket::SocketError error);

  void update();

  void ackAlarms();
  void resetAlarms();

  void sendStart();
  void sendStop();
  void sendTimeout(QString descr, QString cmdname, int ms);

private:
  void setupChart(QObject *object, QString objectName);
  void raiseAlarm(QString message);

  void sendCommand(QString name, QJsonDocument json);

  const QString _host;
  const quint16 _port;

  QTimer _reconnectTimer;
  QThread _broadcastReceiverThread;
  BroadcastReceiver _broadcastReceiver;
  QMutex _buffersMutex;
  QWaitCondition _buffersNotEmpty;
  QByteArray _buffers;
  BroadcastParser _broadcastParser;

  QMap<QString, DataSet> _sets;
  QMap<QString, DataAxis> _axes;
  QTimer _updateTimer;
  //QElapsedTimer _scrollTimer;
  QTime _lastTime;
  QObjectList _alarms;
  QList<CommandObject *> _commands;

  // Append here every Q_PROPERTY that should be exposed to the UI
  // (in alphabetical order)
  bool _connected = false;
  qreal _exhaleInhale = 1.0;
  qreal _exhaleTime = 1600.0;
  qreal _fillTime = 1200.0;
  QString _fsmState = "-";
  qreal _inhaleTime = 1600.0;
  QString _mode = "PC-A/C-PRVC";
  int _patientAge = 58;
  QString _patientName = "John";
  QString _patientSurname = "Smith";
  qreal _patientWeight = 89;
  qreal _pauseTime = 200.0;
  qreal _rpm = 18.8;
  qreal _peep = 5.0;
  qreal _valveInAir = 100.0;
  qreal _valveInhale = 100.0;
  qreal _valveInO2 = 100.0;
  qreal _valveExhale = 100.0;
};

#endif // DATASOURCE_H
