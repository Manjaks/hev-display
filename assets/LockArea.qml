import QtQuick 2.12

MouseArea {
    anchors.fill: parent
    width: parent.width
    height: parent.height
    onPressed: {
        lockSign.x = mouseX - lockSign.width / 2
        lockSign.y = mouseY - lockSign.height / 2
        lockSign.visible = true
    }
    onReleased: lockSign.visible = false
    Rectangle {
        id: lockSign
        color: Style.btnDnColor
        width: parent.width * 0.1
        height: parent.width * 0.1
        visible: false
        radius: 20
        border.width: 2
        border.color: "black"
        Image {
            anchors.centerIn: parent
            source: "svg/lock-solid.svg"
            sourceSize.width: parent.height * 0.8
            sourceSize.height: parent.height * 0.8
        }
    }
}
