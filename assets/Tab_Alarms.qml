import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    Rectangle {
        color: Style.btnDnColor
        anchors.fill: parent
    }

    ScrollView {
        id: scrollView
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.leftMargin: 10

        ScrollBar.vertical.policy: ScrollBar.AlwaysOn

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            spacing: 10
            model: dataSource.alarms

            delegate: Item {
                property QtObject alarm: dataSource.alarms[model.index]
                x: 5
                width: 80
                height: 30
                Row {
                    spacing: 10
                    Image {
                        source: alarm.acked ? 'svg/bell-regular.svg' : 'svg/bell-solid.svg'
                        sourceSize.width: parent.height * 0.8
                        sourceSize.height: parent.height * 0.8
                    }
                    Text {
                        text: alarm.since.toLocaleString("")
                        font.family: Style.fontFamily
                        font.pointSize: Style.fontPointSize
                        font.italic: true
                    }
                    Text {
                        text: alarm.until.toLocaleString("")
                        font.family: Style.fontFamily
                        font.pointSize: Style.fontPointSize
                        font.italic: true
                    }
                    Text {
                        text: alarm.message
                        anchors.verticalCenter: parent.verticalCenter
                        font.family: Style.fontFamily
                        font.pointSize: Style.fontPointSize
                        font.bold: !alarm.acked
                    }
                }
            }
        }
    }
    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        spacing: 10
        RoundButton {
            width: Style.btnHeight * 1.5
            height: Style.btnHeight
            Image {
                anchors.centerIn: parent
                source: "svg/bell-regular.svg"
                sourceSize.height: parent.height * 0.8
            }
            onClicked: dataSource.ackAlarms()
        }
        RoundButton {
            width: Style.btnHeight * 1.5
            height: Style.btnHeight
            Image {
                anchors.centerIn: parent
                source: "svg/bell-solid.svg"
                sourceSize.height: parent.height * 0.8
            }
            onClicked: dataSource.resetAlarms()
        }
    }
}
