import QtQuick 2.12
import QtQuick.Layouts 1.12

Item {
    property string title: "Title"

    Layout.minimumWidth: 200
    Layout.minimumHeight: 100

    Rectangle {
        id: cellBg
        width: parent.width
        height: parent.height
        color: Style.cellBgColor
        radius: 5
        border.color: Style.cellBgColor
        border.width: 5
    }

    ColumnLayout {
        id: cellLayout
        width: parent.width
        height: parent.height

        Text {
            text: parent.parent.title
            color: "white"
            font.family: Style.fontFamily
            font.weight: Font.Bold
            font.pixelSize: 30
            Layout.alignment: Qt.AlignCenter
        }
        Text {
            text: "***"
            color: "white"
            font.family: Style.fontFamily
            font.pixelSize: 25
            Layout.alignment: Qt.AlignCenter
        }
    }
}

