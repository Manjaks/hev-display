TARGET = hev-display
INCLUDEPATH += sources

QT += quick quickcontrols2 svg charts network concurrent

SOURCES += sources/main.cpp \
    sources/datasource.cpp

HEADERS += \
    sources/datasource.h

OTHER_FILES += \
    assets/*.qml \
    assets/svg/*.svg

RESOURCES += \
    assets/resources.qrc
