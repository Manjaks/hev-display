import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    visibility: "Maximized"
    flags: Qt.Window | Qt.WindowTitleHint | Qt.WindowSystemMenuHint | Qt.WindowMinimizeButtonHint | Qt.WindowMaximizeButtonHint | Qt.WindowCloseButtonHint

    color: Style.mainBgColor

    property bool openGL: false
    property bool locked: false

    function notify(message) {
        var notification = Qt.createQmlObject(
         'import QtQuick 2.0; Notification { text: "' + message + '" }',
         notifications,
         "Notifications");
        notification.destroy(3000);
        console.log(message)
    }
    Connections {
        target: dataSource
        onAlarm: console.log("Alarm: " + message);
        onAlarmsChanged: {
            alarmNotification.text = ""
            alarmNotification.visible = false
            for (var i = 0; i < dataSource.alarms.length && i < 5; ++i) {
                if (dataSource.alarms[i].acked) continue
                alarmNotification.text += dataSource.alarms[i].message + ". "
                alarmNotification.visible = true
            }
        }
        onConnectedChanged: notify(dataSource.connected ? "Connected" : "Disconnected")
        onCommandCreated: notify(name + " command created")
        onCommandSent: notify(name + " command sent")
        onCommandAcked: notify(name + " command accepted")
        onCommandFailed: notify(name + " command failed")
        onNotification: notify(message)
    }

    header: Rectangle {
        height: 50

        color: Style.topBgColor

        RoundButton {
            id: modeButton
            text: dataSource.mode
            font.family: Style.fontFamily
            font.weight: Font.Bold
            font.pixelSize: 30
            radius: Style.btnRadius
            width: 250
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 10
            onClicked: modePopup.open()
        }
        Text {
            text: dataSource.patientName + " " + dataSource.patientSurname + ", "
                  + dataSource.patientAge + ", " + dataSource.patientWeight + "kg"
            font.family: Style.fontFamily
            font.weight: Font.DemiBold
            font.pixelSize: 30
            color: "white"
            anchors.left: modeButton.right
            anchors.leftMargin: 20
            anchors.verticalCenter: parent.verticalCenter
        }
        Image {
            id: plug
            source: "svg/plug-solid.svg"
            sourceSize.width: parent.height * 0.8
            sourceSize.height: parent.height * 0.8
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenter: parent.verticalCenter
        }
        ColorOverlay {
            anchors.fill: plug
            source: plug
            color: "white"
        }

        Row {
            anchors.left: parent.horizontalCenter

            Notification {
                id: alarmNotification
                width: applicationWindow.width / 2.1
                alarm: true
                color: "red"
                visible: false
            }
            Text {
                font.family: Style.fontFamily
                font.pointSize: 10
                font.weight: Font.DemiBold
                text: "Version: " + (typeof(version) !== "undefined" ? version : "preview")
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
            }
        }

        LockArea {
            visible: locked
        }
    }

    StackLayout {
        id: stackLayout
        anchors.fill: parent

        currentIndex: tabBar.currentIndex

        Tab_Main {
            id: tabMain
        }
        Tab_Alarms {
            id: tabAlarms
        }
        Tab_Settings {
            id: tabSettings
        }
        Tab_Charts {
            id: tabCharts
            openGL: applicationWindow.openGL
        }
        Tab_Limits {
            id: tabLimits
        }
    }
    Popup {
        id: holdPopup
        width: parent.width * 0.3
        height: parent.height * 0.3
        modal: false
        focus: false
        anchors.centerIn: Overlay.overlay

        background: Rectangle {
            color: Style.mainBgColor
        }
        ProgressBar {
            id: holdProgress
            anchors.fill: parent
            onValueChanged: if (value >= 1.0) holdComplete()
        }
        Timer {
            id: holdTimer
            interval: Style.holdSecs * 1000 / 20
            running: false
            repeat: true
            onTriggered: holdProgress.value += 0.05
        }
        onOpened: {
            holdProgress.value = 0
            holdTimer.start()
        }
    }
    signal holdComplete()
    onHoldComplete: {
        if (lockButton.pressed) {
            toggleLock()
        } else if (connectButton.pressed) {
            if (dataSource.connected) {
                dataSource.broadcastDisconnect()
            } else {
                dataSource.broadcastConnect()
            }
        } else if (startButton.pressed) {
            dataSource.sendStart()
        } else if (stopButton.pressed) {
            dataSource.sendStop()
        }
    }
    function toggleLock() {
        locked = !locked
        notify("Screen " + (locked ? "locked" : "unlocked"))
    }
    property Item digitsItem
    Popup {
        id: digitsPopup
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        anchors.centerIn: Overlay.overlay
        background: Rectangle {
            color: Style.mainBgColor
            radius: Style.btnRadius
        }
        visible: false
        contentItem: Input_Digits {
            id: digitsInput
        }
    }
    function askInputDigits(item, value) {
        digitsItem = item
        digitsInput.initialize(value)
        digitsPopup.open()
    }
    function returnInputDigits(valid, value) {
        digitsPopup.close()
        if (valid) {
            digitsItem.set(value)
        }
    }
    Popup {
        id: modePopup
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
        anchors.centerIn: Overlay.overlay
        background: Rectangle {
            color: Style.mainBgColor
            radius: Style.btnRadius
        }
        visible: false
        contentItem: Input_Mode {
            id: modeInput
        }
    }
    Column {
        id: notifications
        x: parent.width / 3 * 2 - 10
        y: 10
        width: parent.width / 3
        spacing: 10
    }
    LockArea {
        visible: locked
    }

    footer: Item {
        height: applicationWindow.height * 0.1

        RoundButton {
            id: startButton
            height: parent.height * 0.8
            width: height
            radius: 10
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            Image {
                anchors.centerIn: parent
                source: "svg/play-solid.svg"
                sourceSize.width: parent.height * 0.8
                sourceSize.height: parent.height * 0.8
            }
            onPressed: holdPopup.open()
            onReleased: holdPopup.close()
        }
        RoundButton {
            id: stopButton
            height: parent.height * 0.8
            width: height
            radius: 10
            anchors.left: startButton.right
            anchors.leftMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            Image {
                anchors.centerIn: parent
                source: "svg/stop-solid.svg"
                sourceSize.width: parent.height * 0.8
                sourceSize.height: parent.height * 0.8
            }
            onPressed: holdPopup.open()
            onReleased: holdPopup.close()
        }
        TabBar {
            id: tabBar
            position: TabBar.Footer
            anchors.horizontalCenter: parent.horizontalCenter

            contentHeight: parent.height
            currentIndex: 3

            background: Rectangle {
                color: Style.mainBgColor
            }
            Repeater {
                model: [
                    "svg/user-md-solid.svg",
                    "svg/exclamation-triangle-solid.svg",
                    "svg/fan-solid.svg",
                    "svg/chart-area-solid.svg",
                    "svg/sliders-h-solid.svg"
                ]
                TabButton {
                    id: tabBtnMain
                    Image {
                        anchors.centerIn: parent
                        source: model.modelData
                        sourceSize.width: parent.parent.height * 0.8
                        sourceSize.height: parent.parent.height * 0.8
                    }
                    width: parent.height * 2
                    spacing: 1
                    background: Rectangle {
                        color: tabBar.currentIndex === model.index ? Style.btnDnColor : Style.btnUpColor
                    }
                }
            }
        }
        RoundButton {
            id: connectButton
            height: parent.height * 0.8
            width: height
            radius: 10
            anchors.right: lockButton.left
            anchors.rightMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            Image {
                anchors.centerIn: parent
                source: "svg/sign-in-alt-solid.svg"
                visible: !dataSource.connected
                sourceSize.width: parent.height * 0.8
                sourceSize.height: parent.height * 0.8
            }
            BusyIndicator {
                anchors.centerIn: parent
                running: dataSource.connected
            }
            onPressed: holdPopup.open()
            onReleased: holdPopup.close()
        }
        LockArea {
            visible: locked
        }
        RoundButton {
            id: lockButton
            height: parent.height * 0.8
            width: height
            radius: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenter: parent.verticalCenter
            Image {
                anchors.centerIn: parent
                source: locked ? "svg/lock-solid.svg" : "svg/unlock-solid.svg"
                sourceSize.width: parent.height * 0.8
                sourceSize.height: parent.height * 0.8
            }
            onPressed: holdPopup.open()
            onReleased: holdPopup.close()
        }
    }
}
