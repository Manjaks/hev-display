import QtQuick 2.12
import QtGraphicalEffects 1.0

Item {
    property alias text: label.text
    property alias color: rect.color
    property bool alarm: false
    width: parent.width
    height: 50

    RectangularGlow {
        anchors.fill: parent
        glowRadius: 10
        spread: 0.2
        color: "black"
        cornerRadius: glowRadius
    }
    Rectangle {
        id: rect
        anchors.fill: parent
        color: "white"
    }
    Image {
        id: icon
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.verticalCenter: parent.verticalCenter
        source: alarm ? "svg/exclamation-triangle-solid.svg" : "svg/exclamation-circle-solid.svg"
        sourceSize.width: parent.height * 0.8
        sourceSize.height: parent.height * 0.8
    }
    Text {
        id: label
        anchors.left: icon.right
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        font.pointSize: 15
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
}
