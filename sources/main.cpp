#include <QApplication>
#include <QtCore/QCommandLineParser>
#include <QtCore/QLoggingCategory>
#include <QtCore/QRegularExpression>
//#include <QtQuickControls2/QQuickStyle>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlContext>
#include "datasource.h"

#ifndef VERSION
# define VERSION "WIP"
#endif

int main(int argc, char *argv[])
{
  QString host = "localhost";
  quint16 port = 54322;

  QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  //QGuiApplication app(argc, argv);
  // Qt Charts uses Qt Graphics View Framework for drawing, therefore QApplication must be used.
  QApplication app(argc, argv);

  QCommandLineParser cli;
  QCommandLineOption cli_server(QStringList() << "server",
   QApplication::translate("server", "Server address"),
   QApplication::translate("server", "[<host>]:<port>"));
  cli.addOption(cli_server);
  QCommandLineOption cli_size(QStringList() << "size",
   QApplication::translate("size", "Override minimum window size"),
   QApplication::translate("size", "<width>x<height>"));
  cli.addOption(cli_size);
  QCommandLineOption cli_import(QStringList() << "import",
   QApplication::translate("import", "Import data from CSV"),
   QApplication::translate("csv", "<path>.csv"));
  cli.addOption(cli_import);
  QCommandLineOption cli_opengl(QStringList() << "opengl",
   QApplication::translate("opengl", "Enable OpenGL acceleration"));
  cli.addOption(cli_opengl);
  cli.process(app);

  QString cli_server_str = cli.value(cli_server);
  if (cli_server_str.length()) {
    QRegularExpression re_server(
     QLatin1String("([a-z0-9-.]+)?:(\\d+)"), QRegularExpression::CaseInsensitiveOption);
    QRegularExpressionMatch match;
    if (cli_server_str.contains(re_server, &match)) {
      host = match.captured(1);
      if (!host.length()) { host = "localhost"; }
      port = match.captured(2).toUShort();
      qDebug() << "Server: " << host << ":" << port;
    } else {
      qDebug() << "Invalid server '" << cli_server_str << "'";
    }
  }

  //QQuickStyle::setStyle("Fusion");

  QQmlApplicationEngine engine;
  QQmlContext *rootContext = engine.rootContext();
  DataSource dataSource(host, port, nullptr);
  rootContext->setContextProperty("version", QString(VERSION));
  rootContext->setContextProperty("dataSource", &dataSource);
  engine.load(QUrl("qrc:/main.qml"));
  QObject *rootObject = engine.rootObjects().first();
  dataSource.setup(rootObject);

  //QQmlContext *windowContext = engine.contextForObject(rootObject);

  QString cli_import_str = cli.value(cli_import);
  if (cli_import_str.length()) {
    QFile file(cli_import_str);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
      qDebug() << "Unable to open file '" << cli_import_str << "'";
    }
    dataSource.import(file);
  }

  dataSource.fakeAlarm("Oxygen supply fail", 209, 43, true);
  dataSource.fakeAlarm("Battery low", 127, 38);
  dataSource.fakeAlarm("Intentional stop", 91, 1);
  dataSource.fakeAlarm("High VTE", 25, 13);
  dataSource.fakeAlarm("Low PEEP", 4, 2);

  if (cli.isSet(cli_opengl)) {
    rootObject->setProperty("openGL", true);
  }
  QString cli_size_str = cli.value(cli_size);
  if (cli_size_str.length()) {
    QRegularExpression re_size(QLatin1String("(\\d+)x(\\d+)"));
    QRegularExpressionMatch match;
    if (cli_size_str.contains(re_size, &match)) {
      rootObject->setProperty("width", match.captured(1).toInt());
      rootObject->setProperty("height", match.captured(2).toInt());
      qDebug() << "Root size: " << rootObject->property("width") << "x" << rootObject->property("height");
    } else {
      qDebug() << "Invalid size '" << cli_size_str << "'";
    }
  }
  return app.exec();
}
